package com.example.crocusoftweather.dto.weatherDto;

import com.example.crocusoftweather.exception.ErrorObject;
import lombok.Data;

@Data
public class WeatherResponse {

    private Request request;
    private Location location;
    private Current current;

    private boolean success;
    private ErrorObject error;

}
