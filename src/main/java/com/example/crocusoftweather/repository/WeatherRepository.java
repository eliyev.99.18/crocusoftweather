package com.example.crocusoftweather.repository;

import com.example.crocusoftweather.entity.weather.WeatherEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WeatherRepository extends JpaRepository<WeatherEntity,Integer> {

    Optional<WeatherEntity> findByRequestedCityName(String cityName);

}
