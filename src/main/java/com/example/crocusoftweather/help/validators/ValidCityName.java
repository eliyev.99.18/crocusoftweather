package com.example.crocusoftweather.help.validators;

import com.example.crocusoftweather.help.validators.ValidatorCityName;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Constraint(validatedBy = ValidatorCityName.class)
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ValidCityName {
    String message() default "Invalid city name: Provide valid city name please!";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
