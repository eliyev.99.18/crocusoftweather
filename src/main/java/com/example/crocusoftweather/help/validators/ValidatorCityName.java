package com.example.crocusoftweather.help.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.stereotype.Component;

@Component

public class ValidatorCityName implements ConstraintValidator<ValidCityName, String> {
    @Override
    public void initialize(ValidCityName constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {

        String city = s.trim();

        //cityName bosluqdan ibaret String ve ya null vermek olmaz
        if (s == null  || city.isBlank()) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate("City name must not be empty")
                    .addConstraintViolation();
            return false;
        }


//        daxil etdiyimiz cityName ancaq herf olmalidir
        if (!s.matches("^[a-zA-Z]*$")) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate("City name must contain only alphabetic characters")
                    .addConstraintViolation();
            return false;
        }

        return true;
    }


   }



