package com.example.crocusoftweather.client;

import com.example.crocusoftweather.dto.weatherDto.WeatherResponse;

import com.example.crocusoftweather.help.validators.ValidCityName;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "weatherStack", url = "http://api.weatherstack.com")
@Component
public interface WeatherClient {
     @GetMapping("/current")
     WeatherResponse   getCurrentWeatherResponse(@RequestParam ("access_key" ) @ValidCityName String accessKey , @RequestParam("query") String query);





}
