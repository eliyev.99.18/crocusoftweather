package com.example.crocusoftweather.controller;

import com.example.crocusoftweather.dto.weatherDto.WeatherDTO;
import com.example.crocusoftweather.help.validators.ValidCityName;
import com.example.crocusoftweather.service.WeatherService;
import com.example.crocusoftweather.service.impl.WeatherServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/current-weather")
public class WeatherController {

    @Autowired
    private WeatherService weatherService;
    

    @GetMapping("/{city}")
    public ResponseEntity<WeatherDTO> getWeatherData(@PathVariable("city") @ValidCityName String cityName){
        return ResponseEntity.ok(weatherService.getCurrentWeather(cityName));
    }

}
//1
//2+
//3+
//4+
//5
//6
//7+
//8+
//9
//10+
//11
//12+