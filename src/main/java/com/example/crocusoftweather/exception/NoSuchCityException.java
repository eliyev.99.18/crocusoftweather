package com.example.crocusoftweather.exception;

public class NoSuchCityException extends RuntimeException{
    public NoSuchCityException(String message) {
        super(message);
    }
}
