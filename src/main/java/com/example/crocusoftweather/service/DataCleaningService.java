package com.example.crocusoftweather.service;

import org.springframework.stereotype.Service;

@Service
public interface DataCleaningService {
    void eraseOldData();
}
