package com.example.crocusoftweather.service;

import com.example.crocusoftweather.dto.weatherDto.WeatherDTO;

public interface WeatherService {
    WeatherDTO getCurrentWeather(String cityName);

}
