package com.example.crocusoftweather.service.impl;

import com.example.crocusoftweather.repository.WeatherRepository;
import com.example.crocusoftweather.service.DataCleaningService;
import org.springframework.stereotype.Service;

@Service
public class DataCleaningServiceImpl implements DataCleaningService {
    private final WeatherRepository weatherRepository;

    public DataCleaningServiceImpl(WeatherRepository weatherRepository) {
        this.weatherRepository = weatherRepository;
    }

    public void eraseOldData() {
        weatherRepository.deleteAll();
    }
}
