package com.example.crocusoftweather.service.impl;

import ch.qos.logback.core.pattern.parser.Parser;
import com.example.crocusoftweather.client.WeatherClient;
import com.example.crocusoftweather.dto.weatherDto.WeatherDTO;
import com.example.crocusoftweather.dto.weatherDto.WeatherResponse;
import com.example.crocusoftweather.entity.weather.WeatherEntity;
import com.example.crocusoftweather.exception.NoSuchCityException;
import com.example.crocusoftweather.help.formatters.DateAndTimeFormatter;
import com.example.crocusoftweather.repository.WeatherRepository;
import com.example.crocusoftweather.service.WeatherService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Optional;

@Service
@Component
@RequiredArgsConstructor
public class WeatherServiceImpl implements WeatherService {

    private final WeatherClient weatherClient;
    private final WeatherRepository weatherRepository;
    private static final  Logger log = LoggerFactory.getLogger(WeatherService.class);
    private final ModelMapper modelMapper;
    @Autowired
    private DateAndTimeFormatter dateAndTimeFormatter;


    private final String ACCESS_KEY = "3e4fc406e0ce14509c3226ed3511ef1b";



    public WeatherDTO getCurrentWeather(String cityName) {


        Optional<WeatherEntity> weatherEntity = weatherRepository.findByRequestedCityName(cityName);
        WeatherDTO weatherDTO = null;

        //database de melumat var ve vaxtini 40 deq kecmiyib
        if (weatherEntity.isPresent() && !isWeatherDataExpired(weatherEntity.get())) {
            log.info("Weather data found in database for city: " + cityName);
            weatherDTO = convertWeatherEntityToWeatherDTO(weatherEntity.get());

        }


        //databasede melumat var amma vaxtidi kecib databaseden kohne silini yeni save olunur

        else if ( weatherEntity.isPresent() && isWeatherDataExpired((weatherEntity.get()))){
            log.info("Weather data expired in database for city: 0 " + cityName);
            weatherRepository.delete(weatherEntity.get());
            WeatherEntity updateWeatherDataFromApi = weatherRepository.save(generateWeatherEntity(cityName));
            weatherDTO = convertWeatherEntityToWeatherDTO((updateWeatherDataFromApi));
        }
//       databasede melumat yoxdur
        else {
            log.info("Weather data not found in database for City: " + cityName);
            WeatherEntity updatedWeatherDataFromApi = weatherRepository.save(generateWeatherEntity(cityName));
            weatherDTO = convertWeatherEntityToWeatherDTO(updatedWeatherDataFromApi);

        }



        return weatherDTO;

    }



    private WeatherEntity generateWeatherEntity(String cityName) {
        var weatherResponse = weatherClient.getCurrentWeatherResponse(ACCESS_KEY, cityName);
        if ((weatherResponse.getError()) != null) {
            throw new NoSuchCityException("No such city: " + cityName + " found in database");
        }

        WeatherEntity weatherEntity = WeatherEntity.builder()
                .requestedCityName(cityName)
                .cityName(weatherResponse.getLocation().getName())
                .country(weatherResponse.getLocation().getCountry())
                .temperature(weatherResponse.getCurrent().getTemperature())
                .updateTime(LocalDateTime.now())
                .responseLocalTime(dateAndTimeFormatter
                        .parse(weatherResponse.getLocation().getLocaltime(),Locale.getDefault()))

                .build();
        return  weatherEntity;
    }



//    databasedeki melumatin vaxti kecib
    private boolean isWeatherDataExpired(WeatherEntity weatherEntity) {
        return weatherEntity.getUpdateTime().isBefore(LocalDateTime.now().minusMinutes(40));
    }
    //Convert(cevirme) WeatherEntity to WeatherDTO modelMapper ile
    private WeatherDTO convertWeatherEntityToWeatherDTO(WeatherEntity weatherEntity) {
        return  modelMapper.map(weatherEntity, WeatherDTO.class);
    }



    }












